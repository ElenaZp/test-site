function Slider (options) {

    let self = {};


    self.element = options.sliderId;

    let createElement = (tagName, className, text) => {

        let element = document.createElement(tagName);
        element.classList.add(className);

        if(text){
            element.textContent = text;
        }
        return element;
    };

    self.init = () => {

        self.length = options.slideData.length;

        let prevSlide = createElement('div', 'slider-prev');
        prevSlide.classList.add('slider-block');
        let wrapper = createElement('div', 'slider-wrapper');
        let currentSlide = createElement('div', 'slider-current');
        currentSlide.classList.add('slider-block');
        let nextSlide = createElement('div', 'slider-next');
        nextSlide.classList.add('slider-block');
        let control = createElement('div', 'slider-control');
        let indicators = createElement('ol', 'slider-indicators');
        let prev = createElement('button', 'slider-control-prev');
        let iconPrev = createElement('i', 'icon-arrow-carrot-left');
        let next = createElement('button', 'slider-control-next');
        let iconNext = createElement('i', 'icon-arrow-carrot-right');

        let sliders = options.slideData.map(function (slide) {
            let listItem = createElement('li', 'slide');

            let img = createElement('img', 'slide-img');
            img.src = slide.imgUrl;
            listItem.appendChild(img);

            let p = createElement('p', 'slide-text', slide.text);
            listItem.appendChild(p);

            let textName = createElement('div', 'text-name');
            listItem.appendChild(textName);

            let name = createElement('h5', 'slide-name', slide.name);
            textName.appendChild(name);

            let status = createElement('h6', 'slide-status', slide.status);
            textName.appendChild(status);

            let indicator = createElement('li', 'slider-indicator');

            indicators.appendChild(indicator);
            return listItem;

        });

        wrapper.appendChild(prevSlide);
        wrapper.appendChild(currentSlide);
        wrapper.appendChild(nextSlide);
        control.appendChild(prev);
        control.appendChild(next);
        prev.appendChild(iconPrev);
        next.appendChild(iconNext);

        self.element.appendChild(wrapper);
        self.element.appendChild(control);
        self.element.appendChild(indicators);

        self.activeIndex = options.activeIndex;

        return sliders;

    }

    self.sliders = self.init();
    self.sliderCurrent = self.element.getElementsByClassName('slider-current');
    self.sliderNext = self.element.getElementsByClassName('slider-next');
    self.sliderPrev = self.element.getElementsByClassName('slider-prev');
    self.wrapper = self.element.getElementsByClassName('slider-wrapper')[0];
    self.indicators = self.element.getElementsByClassName('slider-indicator');
    self.currentBlockIndex = self.activeIndex;

    self.Blocks = [self.sliderPrev[0], self.sliderCurrent[0], self.sliderNext[0]];

    for (let i =0; i < self.Blocks.length; i++){
        self.Blocks[i].appendChild(self.sliders[i]);
    }
    self.moveIndicator = (activeIndex) => {
        for(let i = 0; i < self.indicators.length; i++ ) {
            self.indicators[i].classList.remove('indicator-active');
        }
        self.indicators[activeIndex].classList.add('indicator-active');
    }

    self.moveIndicator(self.currentBlockIndex);

    self.moveSlider = (activeIndex) => {
        self.wrapper.appendChild(self.Blocks[(self.currentBlockIndex <= 0)  ? 2 : self.currentBlockIndex -1] );
        self.wrapper.appendChild(self.Blocks[self.currentBlockIndex]);
        self.wrapper.appendChild(self.Blocks[(self.currentBlockIndex >= self.Blocks.length-1)  ? 0 : self.currentBlockIndex + 1] );
    }

    self.next = () => {
        self.wrapper.classList.add('animation-next');
        self.wrapper.classList.remove('animation');

        if(self.currentBlockIndex >= self.Blocks.length-1){
            self.currentBlockIndex = 0;
        } else {
            self.currentBlockIndex++;
        }

        setTimeout(() => {
            self.wrapper.classList.add('animation');
            self.wrapper.classList.remove('animation-next');
            self.sliderCurrent[0].remove();
            self.sliderNext[0].remove();
            self.sliderPrev[0].remove();
            self.moveSlider(self.currentBlockIndex);
        },600);

        self.moveIndicator(self.currentBlockIndex);
    }

    self.prev = () => {
        self.wrapper.classList.add('animation-prev');
        self.wrapper.classList.remove('animation');
        self.currentBlockIndex--;
        if(self.currentBlockIndex < 0){
            self.currentBlockIndex = self.Blocks.length-1;
        }
        setTimeout(() => {
            self.wrapper.classList.add('animation');
            self.wrapper.classList.remove('animation-prev');
            self.sliderCurrent[0].remove();
            self.sliderNext[0].remove();
            self.sliderPrev[0].remove();
            self.moveSlider(self.currentBlockIndex)
        },300);
        self.moveIndicator(self.currentBlockIndex);
    }

    let slideLeft = self.element.getElementsByClassName('slider-control-prev')[0],
        slideRight = self.element.getElementsByClassName('slider-control-next')[0];


    slideLeft.addEventListener('click', () => { self.prev()});
    slideRight.addEventListener('click',() => { self.next()});


    return self;

}

