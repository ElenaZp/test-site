//nav

$(document).on("click","#toggle-block", function () {
    $('.button').toggleClass("menu-open");

});

//form
  let inputs =document.querySelectorAll('input');
  let box = document.querySelectorAll('div .label-input');
  let input;

  for (let i = 0; i < inputs.length; i++){
      input= inputs[i];
      input.onkeyup = function () {
          if(this.value == ''){
              box[i].classList.remove('formActive');
          } else {
              box[i].classList.add('formActive');
          }
      };
  }
    document.getElementById('message').onkeyup = function () {
        if(this.value == ''){
            document.getElementById('mes').classList.remove('formActive');
        } else {
            document.getElementById('mes').classList.add('formActive');
        }
    };


//active menu
    $(document).ready(function(){
        var $preloader = $('#page-preloader'),
            $spinner   = $preloader.find('.spinner');
        $spinner.fadeOut();
        $preloader.delay(350).fadeOut('slow');
        $(document).on("click","#open-menu a", function (event) {
            event.preventDefault();
            var id  = $(this).attr('href'),
                top = $(id).offset().top;
            $('body,html').animate({scrollTop: top}, 1500);
            $('#open-menu li.active ').removeClass('active');
            $(this).parent().addClass('active');
            return true;
        });
    });

    // modal
    let modal = document.getElementById('modal');

    new Modal ({
        modalId: modal
        })

//slider
let slideData =[
    {
        imgUrl : 'images/testimonial/2.jpg',
        text : 'These cartridges can be replaced by the printer ink of similar brand. Compatible Inkjet Cartridge will help you to make  extra-ordinary saving with money back guarantee. As soon as the cartridge gets empty the ink that it contains begins to dry',
        name : 'ANTHONY WATKINS',
        status : 'VP of Product, VISA'
    },
    {
        imgUrl : 'images/testimonial/3.jpg',
        text : 'These cartridges can be replaced by the printer ink of similar brand. Compatible Inkjet Cartridge will help you to make  extra-ordinary saving with money back guarantee. As soon as the cartridge gets empty the ink that it contains begins to dry',
        name : 'ANTHONY WATKINS',
        status : 'VP of Product, VISA'
    },
    {
        imgUrl : 'images/testimonial/4.jpg',
        text : 'These cartridges can be replaced by the printer ink of similar brand. Compatible Inkjet Cartridge will help you to make  extra-ordinary saving with money back guarantee. As soon as the cartridge gets empty the ink that it contains begins to dry',
        name : 'ANTHONY WATKINS',
        status : 'VP of Product, VISA'
    }
];

let slider = document.getElementById('slider');
new Slider ({
    sliderId: slider,
    slideData: slideData,
    activeIndex: 1,
    slideWidth: 750
})




