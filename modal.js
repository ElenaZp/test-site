'use strict';
function Modal(option) {

    let self = {};
    self.element = option.modalId;

    self.modalOpen = () => {
        document.getElementsByTagName("body")[0].classList.toggle('show-modal');
        document.getElementsByClassName('modal-info-gif')[0].style.display = 'block';
        document.getElementsByClassName('notification')[0].classList.remove('fail');
        self.modalInit();
    }

    self.modalClose = () => {
        document.getElementsByTagName("body")[0].classList.toggle('show-modal');
        let infoText = document.getElementsByClassName('modal-info-text')[0];
        infoText.remove();
    }

    self.modalInit = () => {
        function get(url) {
            return new Promise((resolve, reject) => {
                let xhr = new XMLHttpRequest();
                xhr.open('GET', url, true);
                xhr.onload = () =>{
                    if (xhr.status < 400) {
                        resolve(xhr.response);
                    } else {
                        reject(new Error(xhr.statusText));
                    }
                };

                xhr.onerror = () =>{
                    reject(new Error ('Network Error'))
                };
                xhr.send();
            });
        }

        let links = ['http://httpstat.us/404', 'http://httpstat.us/200','http://httpstat.us/403'];

        get(links[Math.round(Math.random()*2)])
            .then (
                response => {
                    let notificationIndicator = document.getElementsByClassName('notification-status')[0];
                    notificationIndicator.innerHTML = ('Awesome! Request completed successfully');
                    document.getElementsByClassName('notification')[0].classList.add('show-element', 'success');
                    document.getElementsByClassName('modal-info-gif')[0].style.display = 'none';
                    setTimeout( () =>
                        document.getElementsByClassName('notification')[0].classList.remove('show-element', 'success'), 3000 );
                    return response
                },
                reject => {
                    let notificationIndicator = document.getElementsByClassName('notification-status')[0];
                    notificationIndicator.innerHTML = ('Oh no! The request failed');
                    document.getElementsByClassName('notification')[0].classList.add('show-element', 'fail');
                    document.getElementsByClassName('modal-info-gif')[0].style.display = 'none';
                    document.getElementsByTagName("body")[0].classList.toggle('show-modal');
                    throw reject
                 }
            )
            .then ( user => get('https://jsonplaceholder.typicode.com/todos/'))
            .then( userInfo => {
                userInfo = JSON.parse(userInfo);

                function getObj(arr, value1, value2) {

                    function filterByID(item) {
                        if (item.id >= value1 &&  item.id <= value2) {
                            return true;
                        }
                        return false;
                    }
                    let arrUser = arr.filter(filterByID);
                    return arrUser;
                }

                let userInfoText = document.createElement('div');
                userInfoText.className = 'modal-info-text';

                for (let i = 1; i <= 10; i++){
                    let p = document.createElement('p');
                    p.style.display = 'none';
                    p.className = 'control'+i;
                    userInfoText.appendChild(p);
                }
                let p = userInfoText.childNodes;
                p[0].innerHTML = JSON.stringify(getObj(userInfo, 0, 9));
                p[1].innerHTML = JSON.stringify(getObj(userInfo, 10, 19));
                p[2].innerHTML = JSON.stringify(getObj(userInfo, 20, 29));
                p[3].innerHTML = JSON.stringify(getObj(userInfo, 30, 39));
                p[4].innerHTML = JSON.stringify(getObj(userInfo, 40, 49));
                p[5].innerHTML = JSON.stringify(getObj(userInfo, 50, 59));
                p[6].innerHTML = JSON.stringify(getObj(userInfo, 60,69));
                p[7].innerHTML = JSON.stringify(getObj(userInfo, 70, 79));
                p[8].innerHTML = JSON.stringify(getObj(userInfo, 80, 89));
                p[9].innerHTML = JSON.stringify(getObj(userInfo, 90, 99));

               document.getElementsByClassName('modal-info')[0].appendChild(userInfoText);
            },
                reject => {throw reject}
            )

    }
    document.getElementsByClassName('modal-output')[0].addEventListener('click', () => {self.modalClose()});
    document.getElementById('modal-open').addEventListener('click', () => { self.modalOpen()});

    return self;
}



//test